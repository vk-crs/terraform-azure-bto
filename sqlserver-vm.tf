locals {
  restore_db_script = data.template_file.cloudconfig.rendered
}

# Generate random string for storage account
resource "random_string" "storage-account-random" {
  length  = 3
  special = false
  lower   = true
  upper   = false
  number  = false
}

# Generate random password
resource "random_password" "sqlserver-vm-password" {
  length           = 16
  min_upper        = 2
  min_lower        = 2
  min_special      = 2
  number           = true
  special          = true
  override_special = "!@#$%&"
}

# Create Network Security Group to Access VM from Internet
resource "azurerm_network_security_group" "sqlserver-vm-nsg" {
  name                = "nsg-sqlserver-${var.app_name}-${var.environment}"
  location            = azurerm_resource_group.rg-environment.location
  resource_group_name = azurerm_resource_group.rg-environment.name

  tags = {
    application = var.app_name
    environment = var.environment
  }
}

resource "azurerm_network_security_rule" "allow_3389" {
  name                       = "AllowRDP"
  description                = "Allow RDP"
  priority                   = 100
  direction                  = "Inbound"
  access                     = "Allow"
  protocol                   = "Tcp"
  source_port_range          = "*"
  destination_port_range    = "*"
  source_address_prefix      = "Internet"
  destination_address_prefix = "*"
  resource_group_name         = azurerm_resource_group.rg-environment.name
  network_security_group_name = azurerm_network_security_group.sqlserver-vm-nsg.name
}

resource "azurerm_network_security_rule" "allow_5985_5986" {
  name                        = "AllowHTTPHTTPSWinRM"
  description                 = "Allow HTTP/HTTPS WinRM"
  priority                    = 200
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    = "Tcp"
  source_port_range           = "*"
  destination_port_ranges     = ["5985-5986"]
  source_address_prefix       = "Internet"
  destination_address_prefix  = "*"
  resource_group_name         = azurerm_resource_group.rg-environment.name
  network_security_group_name = azurerm_network_security_group.sqlserver-vm-nsg.name
}

# Associate the NSG with the Public Subnet
resource "azurerm_subnet_network_security_group_association" "sqlserver-nsg-association" {
  subnet_id                 = azurerm_subnet.network-public-subnet.id
  network_security_group_id = azurerm_network_security_group.sqlserver-vm-nsg.id
}

# Get a Static Public IP
resource "azurerm_public_ip" "sqlserver-ip" {
  name                = "${var.sqlserver_vm_hostname}-ip"
  location            = azurerm_resource_group.rg-environment.location
  resource_group_name = azurerm_resource_group.rg-environment.name
  allocation_method   = "Static"

  tags = {
    application = var.app_name
    environment = var.environment
  }
}

# Create Network Card for VM
resource "azurerm_network_interface" "sqlserver-vm-nic" {
  name                      = "${var.sqlserver_vm_hostname}-nic"
  location                  = azurerm_resource_group.rg-environment.location
  resource_group_name       = azurerm_resource_group.rg-environment.name

  ip_configuration {
    name                          = "internal"
    subnet_id                     = azurerm_subnet.network-public-subnet.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.sqlserver-ip.id
  }

  tags = {
    application = var.app_name
    environment = var.environment
  }
}

data "template_file" "cloudconfig" {
    template = "${file("scripts/initialization-script.ps1")}"
    vars = {
      disk_sqldata_letter              = var.disk_sqldata_letter
      disk_sqllogs_letter              = var.disk_sqllogs_letter      
    }
}

resource "azurerm_windows_virtual_machine" "sqlserver-vm" {
  name                  = var.sqlserver_vm_hostname
  location              = azurerm_resource_group.rg-environment.location
  resource_group_name   = azurerm_resource_group.rg-environment.name
  size                  = var.sqlserver_vm_size
  network_interface_ids = [azurerm_network_interface.sqlserver-vm-nic.id]

  computer_name         = var.sqlserver_vm_hostname
  admin_username        = var.sqlserver_vm_admin_username
  admin_password        = random_password.sqlserver-vm-password.result
  custom_data           = "${base64encode(data.template_file.cloudconfig.rendered)}"

  os_disk {
    name                 = "sqlserver-${var.sqlserver_vm_hostname}-os-disk"
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
    disk_size_gb         = var.sqlserver_disk_os_size
  }

  source_image_reference {
    publisher = "MicrosoftSQLServer"
    offer     = "SQL2016SP2-WS2016"
    sku       = var.sqlserver_vm_sku
    version   = "latest"
  }

  enable_automatic_updates = true
  provision_vm_agent       = true

  tags = {
    application = var.app_name
    environment = var.environment
  }
}

resource "azurerm_managed_disk" "disk-sqllogs" {
  name                 = "${var.sqlserver_vm_hostname}-disk-sqllogs"
  location              = azurerm_resource_group.rg-environment.location
  resource_group_name   = azurerm_resource_group.rg-environment.name
  storage_account_type = "Standard_LRS"
  create_option        = "Empty"
  disk_size_gb         = var.sqlserver_disk_sqllogs_size

  tags = {
    environment = var.environment
  }
}

resource "azurerm_virtual_machine_data_disk_attachment" "disk-sqllogs-attachment" {
  depends_on=[azurerm_managed_disk.disk-sqllogs]
  managed_disk_id    = azurerm_managed_disk.disk-sqllogs.id
  virtual_machine_id = azurerm_windows_virtual_machine.sqlserver-vm.id
  lun                = "10"
  caching            = "ReadWrite"
}

resource "azurerm_managed_disk" "disk-sqldata" {
  name                 = "${var.sqlserver_vm_hostname}-disk-sqldata"
  location              = azurerm_resource_group.rg-environment.location
  resource_group_name   = azurerm_resource_group.rg-environment.name
  storage_account_type = "Standard_LRS"
  create_option        = "Empty"
  disk_size_gb         = var.sqlserver_disk_sqldata_size

  tags = {
    environment = var.environment
  }
}

resource "azurerm_virtual_machine_data_disk_attachment" "disk-sqldata-attachment" {
  depends_on=[azurerm_managed_disk.disk-sqldata]
  managed_disk_id    = azurerm_managed_disk.disk-sqldata.id
  virtual_machine_id = azurerm_windows_virtual_machine.sqlserver-vm.id
  lun                = "20"
  caching            = "ReadWrite"
}

resource "azurerm_storage_account" "storage-account" {
  name                     = "sa${var.company}${var.environment}${var.sqlserver_vm_hostname}${random_string.storage-account-random.result}"
  resource_group_name      = azurerm_resource_group.rg-environment.name
  location                 = azurerm_resource_group.rg-environment.location
  account_tier             = "Standard"
  account_replication_type = "LRS"
}

data "azurerm_storage_account_sas" "storage-account-sas" {
  connection_string = azurerm_storage_account.storage-account.primary_connection_string
  https_only        = true

  resource_types {
    service   = true
    container = true
    object    = true
  }

  services {
    blob  = true
    queue = false
    table = false
    file  = true
  }

  start = formatdate("YYYY-MM-DD",  timestamp())
  expiry = formatdate("YYYY-MM-DD", timeadd(timestamp(), "24h"))

  permissions {
    read    = true
    write   = true
    delete  = true
    list    = true
    add     = true
    create  = true
    update  = true
    process = true
  }
}

resource "azurerm_storage_container" "scripts-container" {
  name                  = "${var.sqlserver_vm_hostname}scriptscontainer"
  storage_account_name  = azurerm_storage_account.storage-account.name
  container_access_type = "container"
}

resource "azurerm_storage_blob" "initialization-script" {
  name                   = "initialization-script.ps1"
  storage_account_name   = azurerm_storage_account.storage-account.name
  storage_container_name = azurerm_storage_container.scripts-container.name
  type                   = "Block"
  source_content         = local.restore_db_script
}

# Windows VM virtual machine extension - Configure Disks and Restore Database
resource "azurerm_virtual_machine_extension" "sqlserver-vm-extension" {
  depends_on=[
    azurerm_windows_virtual_machine.sqlserver-vm,
    azurerm_managed_disk.disk-sqldata,
    azurerm_managed_disk.disk-sqllogs,
    azurerm_virtual_machine_data_disk_attachment.disk-sqldata-attachment,
    azurerm_virtual_machine_data_disk_attachment.disk-sqllogs-attachment,
    azurerm_storage_blob.initialization-script
  ]

  name                 = "${var.sqlserver_vm_hostname}-vm-extension"
  virtual_machine_id   = azurerm_windows_virtual_machine.sqlserver-vm.id
  publisher            = "Microsoft.Compute"
  type                 = "CustomScriptExtension"
  type_handler_version = "1.9"
  settings = <<SETTINGS
  {
    "fileUris": ["${azurerm_storage_blob.initialization-script.url}${data.azurerm_storage_account_sas.storage-account-sas.sas}&sr=b"],
    "commandToExecute": "powershell.exe -ExecutionPolicy unrestricted -NonInteractive -File initialization-script.ps1"

  }
  SETTINGS
  tags = {
    environment = var.environment
  }
}
