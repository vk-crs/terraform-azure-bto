###############################################################################
# Common
###############################################################################
subscription_id     = "3d54f043-a573-403d-8ac6-9f4e63ce1855"
company             = "corus"
app_name            = "bto"
environment         = "poc"
location            = "east us 2"

###############################################################################
# Network
###############################################################################
network_vnet_cidr   = "10.0.0.0/24"
public_subnet_cidr  = "10.0.0.0/25"
private_subnet_cidr = "10.0.0.128/25"

###############################################################################
# Sql Server VM
###############################################################################
sqlserver_vm_hostname         = "patricstar"
sqlserver_vm_sku              = "SQLDEV"
sqlserver_vm_size             = "Standard_D2s_v3"
sqlserver_vm_admin_username   = "btodbo"
sqlserver_disk_os_size        = 127
sqlserver_disk_sqldata_size   = 32
sqlserver_disk_sqllogs_size   = 32
