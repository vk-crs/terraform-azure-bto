function Prepare-Disk() {
  Param ([Parameter(Mandatory=$true)][string]$disk_sqldata_letter,
         [Parameter(Mandatory=$true)][string]$disk_sqllogs_letter
       )

    Get-Disk | Where partitionstyle -eq 'raw' | Initialize-Disk -PartitionStyle MBR -PassThru | New-Partition -AssignDriveLetter -UseMaximumSize | Format-Volume -FileSystem NTFS -Confirm:$false;
    Set-Volume -DriveLetter ${disk_sqldata_letter}  -NewFileSystemLabel "SQL Data";
    Set-Volume -DriveLetter ${disk_sqllogs_letter}  -NewFileSystemLabel "SQL Logs";
}

function Enable-WinRM(){

  $url = "https://raw.githubusercontent.com/ansible/ansible/devel/examples/scripts/ConfigureRemotingForAnsible.ps1"
  $file = "$env:temp\ConfigureRemotingForAnsible.ps1"
  (New-Object -TypeName System.Net.WebClient).DownloadFile($url, $file)
  powershell.exe -ExecutionPolicy ByPass -File $file
  Enable-WSManCredSSP -Role "Server" -Force
}

function LogMessage(){
  Param ([Parameter(Mandatory=$true)][string]$Message)
  Write-Output $Message;
}

try {
  Start-Transcript -Path 'C:/SetupLog/initialization-terraform.log'
  # Prepare Log and Data Partitions
  Prepare-Disk -disk_sqldata_letter "${disk_sqldata_letter}" -disk_sqllogs_letter "${disk_sqllogs_letter}"

  # Enable WinrRM Connections that will be used later to run script configurations in the host.
  Enable-WinRM
  Stop-Transcript;
  exit 0;
}
Catch {
  Write-Host "An error occurred.";
  Write-Host $_;
  exit 1;
};
